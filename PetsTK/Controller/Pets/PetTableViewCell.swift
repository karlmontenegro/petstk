//
//  PetTableViewCell.swift
//  PetsTK
//
//  Created by Karl Montenegro on 14/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol Favorite {
    func confirmation(op: String, success:Bool)
}

class PetTableViewCell: UITableViewCell {

    let baseURL: String = "http://development.tektonlabs.com/pets-tk-app"
    
    @IBOutlet weak var petImage: UIImageView!
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var petOwnerUsername: UILabel!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var is_favorite = false
    var pet:Pet?
    var delegate:Favorite?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       self.favoriteButton.setImage(UIImage(named: "Like-Disabled-25px.png"), forState: UIControlState.Normal)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favoriteTapped(sender: UIButton) {
        
        if self.is_favorite == false {
            self.setFavorite(){
                json, error in
                if error == nil {
                    self.favoriteButton.setImage(UIImage(named: "Like-Enabled-25px.png"), forState: UIControlState.Normal)
                    self.is_favorite = true
                    self.delegate?.confirmation("like",success: true)
                } else {
                    self.delegate?.confirmation("like",success: false)
                }
            }
            
        } else {
            self.unsetFavorite(){
                json, error in
                if error == nil {
                    self.favoriteButton.setImage(UIImage(named: "Like-Disabled-25px.png"), forState: UIControlState.Normal)
                    self.delegate?.confirmation("unlike", success: true)
                    self.is_favorite = false
                } else {
                    self.delegate?.confirmation("unlike",success: false)
                }
            }
        }
    }
    
    //MARK - Aux Functions
    
    func setState(val: Bool) {
        self.is_favorite = val
        if val {
            self.favoriteButton.setImage(UIImage(named: "Like-Enabled-25px.png"), forState: UIControlState.Normal)
        } else {
            self.favoriteButton.setImage(UIImage(named: "Like-Disabled-25px.png"), forState: UIControlState.Normal)
        }
    }
    
    private func setFavorite(completionHandler: (JSON?,NSError?)->()) {
        
        let parameters = [
            "user_id" : self.defaults.integerForKey("userId")
        ]
        
        Alamofire.request(.POST, baseURL + "/pets/" + self.pet!.id!.description + "/add_favorite.json", parameters: parameters, encoding: .JSON)
            .responseJSON{
                response in
                switch response.result {
                case .Success:
                    let json = JSON(data: response.data!)
                    completionHandler(json, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                    break
                }
        }
        
    }

    private func unsetFavorite(completionHandler: (JSON?,NSError?)->()){
        
        let parameters = [
            "user_id" : self.defaults.integerForKey("userId")
        ]
        
        Alamofire.request(.DELETE, baseURL + "/pets/" + self.pet!.id!.description + "/remove_favorite.json", parameters: parameters, encoding: .JSON)
            .responseJSON{
                response in
                switch response.result {
                case .Success:
                    let json = JSON(data: response.data!)
                    completionHandler(json, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                    break
                }
        }
        
    }
    
    private func alertMessage(winMessage: String, winTitle: String){
        let alertController = UIAlertController(title: winTitle, message: winMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertController) -> Void in
        }))
        
        //self.presentViewController(alertController, animated: true, completion: nil)
    }
}
