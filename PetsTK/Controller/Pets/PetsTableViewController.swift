//
//  PetsTableViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 14/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol Update {
    func refreshData()
}

class PetsTableViewController: UITableViewController,Favorite {

    let baseURL: String = "http://development.tektonlabs.com/pets-tk-app"
    let defaults = NSUserDefaults.standardUserDefaults()
    var petList: Array<Pet> = Array<Pet>()
    var updateDelegate:Update?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.petsRequest(self.defaults.integerForKey("userId"))
        
        //Pull to refresh
        self.refreshControl?.addTarget(self, action: "refreshPets:", forControlEvents: UIControlEvents.ValueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func refreshPets(refreshControl: UIRefreshControl) {
        self.petsRequest(self.defaults.integerForKey("userId"))
        self.tableView.reloadData()
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.petList.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("petCell", forIndexPath: indexPath) as! PetTableViewCell

        let pet = self.petList[indexPath.row]
        let url = NSURL(string: pet.image_url!)
        var placeholderName = ""
        
        cell.petName.text = pet.name
        cell.petOwnerUsername.text = pet.family
        cell.delegate = self
        
        if pet.family! == "DOG" {
            placeholderName = "Dog-Placeholder-100px"
        } else {
            placeholderName = "Cat-Placeholder-100px"
        }
        
        cell.petImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: placeholderName)?.alpha(0.3))
        cell.pet = pet
        
        return cell
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    func confirmation(op: String, success: Bool) {
        if success {
            
            if op == "like" {
                self.alertMessage("Added to My Favorite Pets", winTitle: "Success!")
            } else {
                self.alertMessage("Deleted from My Favorite Pets", winTitle: "Success!")
            }
        } else {
            self.alertMessage("Something went wrong, try again", winTitle: "Error")
        }
        self.updateDelegate?.refreshData()
    }
    
    
    @IBAction func logoutTapped(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Logout", message:
            "Do you want to exit PetsTK?", preferredStyle: UIAlertControllerStyle.Alert)
        
        // Logout action
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertController) -> Void in
            // Logs out
            self.performSegueWithIdentifier("logoutSegue", sender: self)
        }))
        
        // Cancel action
        alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (alertController) -> Void in
            
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "petInfoSegue" {
            let viewController = segue.destinationViewController as! PetInfoViewController
            
            let indexPath = self.tableView.indexPathForSelectedRow
            
            viewController.pet = self.petList[indexPath!.row]
        }
        
    }
    
    @IBAction func unwindToPets(segue: UIStoryboardSegue) {
        
        self.petList.removeAll()
        self.petsRequest(self.defaults.integerForKey("userId"))
        
    }
    
    //MARK: - Auxiliary Functions
    
    private func petsRequest(id: Int) {
        
        Alamofire.request(.GET, baseURL + "/pets.json?user_id=" + id.description).responseJSON{ response in
            switch response.result {
            case .Success:
                let json = JSON(data: response.data!)
                self.petList = DL_Pet().map(json)
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            case .Failure: break
            }
        }
    }
    
    private func alertMessage(winMessage: String, winTitle: String){
        let alertController = UIAlertController(title: winTitle, message: winMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertController) -> Void in
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

