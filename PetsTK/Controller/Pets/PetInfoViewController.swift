//
//  PetInfoViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 20/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import SDWebImage

class PetInfoViewController: UIViewController {

    @IBOutlet weak var petImage: UIImageView!
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var petFamily: UILabel!
    
    var pet:Pet?
    var placeholderName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.petName.text = self.pet!.name
        self.petFamily.text = self.pet!.family
        
        let url = NSURL(string: self.pet!.image_url!)
        
        if pet!.family! == "DOG" {
            placeholderName = "Dog-Placeholder-100px"
        } else {
            placeholderName = "Cat-Placeholder-100px"
        }
        
        self.petImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: placeholderName)?.alpha(0.3))
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
