//
//  InitialPageViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 14/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit

class InitialPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    private lazy var viewList: [UIViewController] =
        [UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewControllerWithIdentifier("PageOne"),
         UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewControllerWithIdentifier("PageTwo"),
         UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewControllerWithIdentifier("PageThree"),
         UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewControllerWithIdentifier("PageFour")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        
        self.setViewControllers([self.viewList[0]], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let index: Int = self.viewList.indexOf(viewController)!
        
        if index == 0 {
            return nil
        } else {
            return self.viewList[index-1]
        }
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let index: Int = self.viewList.indexOf(viewController)!
        
        if index < self.viewList.count - 1 {
            return self.viewList[index+1]
        } else {
            return nil
        }
        
    }

    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is UIScrollView {
                subView.frame = self.view.bounds
            } else if subView is UIPageControl {
                self.view.bringSubviewToFront(subView)
            }
        }
        super.viewDidLayoutSubviews()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}