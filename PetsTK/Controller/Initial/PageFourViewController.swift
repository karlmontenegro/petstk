//
//  PageFourViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 14/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PageFourViewController: UIViewController {
    
    let baseURL: String = "http://development.tektonlabs.com/pets-tk-app"
    let defaults = NSUserDefaults.standardUserDefaults()
    var user: User = User()
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activitySpinner.hidden = true
        self.activitySpinner.hidesWhenStopped = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginTapped(sender: AnyObject) {
        
        self.activitySpinner.hidden = false
        self.activitySpinner.startAnimating()
        
        self.loginRequest(self.txtEmail.text!, password: self.txtPassword.text!){
            json, error in
            
            if error == nil {
                self.defaults.setInteger(json!["id"].int!, forKey: "userId")
                
                self.user.id = json!["id"].int
                self.user.first_name = json!["first_name"].stringValue
                self.user.last_name = json!["last_name"].stringValue
                self.user.email = json!["email"].stringValue
                
                self.activitySpinner.stopAnimating()
                self.performSegueWithIdentifier("mainViewSegue", sender: self)
                
            } else {
                self.activitySpinner.stopAnimating()
                self.alertMessage("Email/Password not found", winTitle: "Error")
            }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "mainViewSegue" {
            let tabController = segue.destinationViewController as! UITabBarController
            let navController = tabController.viewControllers![0]
            let viewController = navController.childViewControllers.first as! ProfileViewController
            
            viewController.user = self.user
        }
    }
    
    
    
    // MARK: - Auxiliary functions
    
    private func loginRequest(email: String, password: String, completionHandler:(JSON?,NSError?)->()) {
        
        let parameter = [
            "email" : email,
            "password" : password
        ]
        
        Alamofire.request(.POST, baseURL + "/users/sign_in.json", parameters: parameter)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    let json = JSON(data: response.data!)
                    completionHandler(json,nil)
  
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    private func alertMessage(winMessage: String, winTitle: String){
        let alertController = UIAlertController(title: winTitle, message: winMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertController) -> Void in
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}



