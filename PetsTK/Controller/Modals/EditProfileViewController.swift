//
//  EditProfileViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 19/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditProfileViewController: UIViewController {

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    
    let baseURL: String = "http://development.tektonlabs.com/pets-tk-app"
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstName.text = self.user!.first_name
        self.lastName.text = self.user!.last_name
        self.email.text = self.user!.email
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveTapped(sender: AnyObject) {
        
        self.saveDataRequest(){
            json, error in
            
            if error == nil {
                self.user?.id = json!["id"].int
                self.user?.first_name = json!["first_name"].stringValue
                self.user?.last_name = json!["last_name"].stringValue
                self.user?.email = json!["email"].stringValue
            
                self.performSegueWithIdentifier("unwindToProfileSegue", sender: self)
            }
        }
        
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    
    // MARK: - Auxiliary Functions
    
    private func saveDataRequest(completionHandler: (JSON?, NSError?)->()){
        
        let parameters = [
            "email"    : self.email.text!,
            "first_name" : self.firstName.text!,
            "last_name" : self.lastName.text!
        ]
        
        Alamofire.request(.PUT, baseURL + "/users/" + self.user!.id!.description + ".json", parameters: parameters,  encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    let json = JSON(data: response.data!)
                    completionHandler(json, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                    break
                }
        }
    }
    
}
