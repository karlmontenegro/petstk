//
//  NewPetViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 17/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewPetViewController: UIViewController {

    @IBOutlet weak var petName: UITextField!
    @IBOutlet weak var petFamily: UITextField!
    @IBOutlet weak var petURL: UITextField!
    
    let baseURL: String = "http://development.tektonlabs.com/pets-tk-app"
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be 
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func saveTapped(sender: UIButton) {
        
        self.newPet(){
            json, error in
            if error == nil {
                self.performSegueWithIdentifier("unwindToPetsSegue", sender: self)
            }
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "unwindToPetsSegue" {
            
        }
    }
    

    //MARK: - Auxiliary Functions
    
    private func newPet(completionHandler: (JSON?,NSError?)->()) {
        
        let parameters = [
            "name"    : self.petName.text!,
            "family" : self.petFamily.text!,
            "user_id" : self.defaults.integerForKey("userId"),
            "image_url" : self.petURL.text!
        ]
        
        Alamofire.request(.POST, baseURL + "/pets.json", parameters: parameters as? [String : AnyObject], encoding: .JSON)
            .validate()
            .responseJSON{ response in
            switch response.result {
            case .Success:
                let json = JSON(data: response.data!)
                completionHandler(json,nil)
            case .Failure(let error):
                completionHandler(nil,error)
            }
        }
    }
}
