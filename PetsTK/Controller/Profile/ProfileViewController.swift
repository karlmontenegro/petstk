//
//  ProfileViewController.swift
//  PetsTK
//
//  Created by Karl Montenegro on 19/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DZNEmptyDataSet
import SDWebImage

class ProfileViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, Update {

    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var favPetTableView: UITableView!
    
    let baseURL: String = "http://development.tektonlabs.com/pets-tk-app"
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var user: User? = nil
    var favPetList:Array<Pet> = Array<Pet>()
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fullName.text = (self.user?.first_name!)! + " " + (self.user?.last_name!)!
        
        self.email.text = self.user?.email
        
        // Favorite pet list
        
        self.favoritePetsRequest()
        
        //Empty Data Set
        self.favPetTableView.emptyDataSetDelegate = self
        self.favPetTableView.emptyDataSetSource = self
        self.favPetTableView.tableFooterView = UIView()
        
        //Pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Liked Pets")
        
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        
        self.favPetTableView.addSubview(refreshControl)
        
        (self.navigationController?.tabBarController?.childViewControllers[1].childViewControllers.first as! PetsTableViewController).updateDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.favoritePetsRequest()
    }
    
    func refreshData() {
        self.favoritePetsRequest()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.favPetList.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("favoritePet", forIndexPath: indexPath) as! FavoritePetTableViewCell
        
        let pet = self.favPetList[indexPath.row]
        let url = NSURL(string: pet.image_url!)
        var placeholderName = ""
        
        
        cell.petName.text = pet.name
        cell.petFamily.text = pet.family
        
        if pet.family! == "DOG" {
            placeholderName = "Dog-Placeholder-100px"
        } else {
            placeholderName = "Cat-Placeholder-100px"
        }
        
        cell.petImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: placeholderName)?.alpha(0.3))
        
        return cell
    }
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let alertController = UIAlertController(title: "Attention", message:
                "Do you want to delete this pet?", preferredStyle: UIAlertControllerStyle.Alert)
            
            // Delete action
            alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { (alertController) -> Void in
                
                // Deletes the favorite
                
                self.unsetFavorite(self.favPetList[indexPath.row].id!){
                    json, error in
                    if error == nil {
                        self.favPetList.removeAtIndex(indexPath.row)
                        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                        
                    } else {
                    }
                }
                self.favPetTableView.reloadData()
            }))
            
            // Cancel action
            alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Default,handler: { (alertController) -> Void in
                self.favPetTableView.reloadData()
            }))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    
    
    // Setup for the empty data set information
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "No-Pets-100px")
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView!) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "You have no favorite pets"
        let attr = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)]
        return NSAttributedString(string: str, attributes: attr)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        var str = "Go to pets tab and start liking some pets!"
        let attr = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleFootnote)]
        return NSAttributedString(string: str, attributes: attr)
    }

    
    @IBAction func unwindToProfile(segue: UIStoryboardSegue) {
        
        if let editProfileView = segue.sourceViewController as? EditProfileViewController {
            
            self.fullName.text = editProfileView.user!.first_name! + " " + editProfileView.user!.last_name!
            self.email.text = editProfileView.user!.email
        }
        
    }
    @IBAction func logoutTapped(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Logout", message:
            "Do you want to exit PetsTK?", preferredStyle: UIAlertControllerStyle.Alert)
        
        // Logout action
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertController) -> Void in
            // Logs out
            self.performSegueWithIdentifier("logoutSegue", sender: self)
        }))
        
        // Cancel action
        alertController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default,handler: { (alertController) -> Void in
            
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "editUserSegue" {
            let viewController = segue.destinationViewController as! EditProfileViewController
            viewController.user = self.user
        }
        
        if segue.identifier == "petDetailSegue" {
            let viewController = segue.destinationViewController as! PetInfoViewController
            let indexPath = self.favPetTableView.indexPathForSelectedRow
            
            viewController.pet = self.favPetList[indexPath!.row]
        }
    }

    // MARK: - Auxiliary Functions
    
    private func favoritePetsRequest(){
        Alamofire.request(.GET, baseURL + "/users/" + self.defaults.integerForKey("userId").description + "/favorite_pets.json")
            .responseJSON{
                response in
                switch response.result {
                case .Success:
                    let json = JSON(data: response.data!)
                    self.favPetList = DL_Pet().map(json)
                    self.favPetTableView.reloadData()
                    self.refreshControl.endRefreshing()
                case .Failure:
                    break
                }
        }
    }
    
    private func unsetFavorite(id: Int, completionHandler: (JSON?,NSError?)->()){
        
        let parameters = [
            "user_id" : self.defaults.integerForKey("userId")
        ]
        
        Alamofire.request(.DELETE, baseURL + "/pets/" + id.description + "/remove_favorite.json", parameters: parameters, encoding: .JSON)
            .responseJSON{
                response in
                switch response.result {
                case .Success:
                    let json = JSON(data: response.data!)
                    completionHandler(json, nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                    break
                }
        }
        
    }
    
}
