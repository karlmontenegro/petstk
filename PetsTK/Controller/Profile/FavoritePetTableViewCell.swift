//
//  FavoritePetTableViewCell.swift
//  PetsTK
//
//  Created by Karl Montenegro on 20/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import UIKit

class FavoritePetTableViewCell: UITableViewCell {

    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var petFamily: UILabel!
    @IBOutlet weak var petImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
