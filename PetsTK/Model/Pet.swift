//
//  Pet.swift
//  PetsTK
//
//  Created by Karl Montenegro on 18/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import Foundation

class Pet {
    
    var id: Int?
    var name: String?
    var family: String?
    var user_id: Int?
    var image_url: String?
    var is_favorite: Bool?
    var url: String?
    
    init(id: Int?, name: String?, family: String?, user_id: Int?, image_url: String?, is_favorite: Bool?, url: String?) {
        
        self.id = id
        self.name = name
        self.family = family
        self.user_id = user_id
        self.image_url = image_url
        self.is_favorite = is_favorite
        self.url = url
        
    }
}
