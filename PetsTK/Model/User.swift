//
//  User.swift
//  PetsTK
//
//  Created by Karl Montenegro on 19/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import Foundation

class User {
    
    var id:Int?
    var first_name:String?
    var last_name:String?
    var email:String?
    var created_at:NSDate?
    var updated_at:NSDate?
    
    init(){
        
    }
    
    init(id: Int?, first_name: String?, last_name: String?, email: String?, created_at: NSDate?, updated_at: NSDate?){
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.created_at = created_at
        self.updated_at = updated_at
    }
    
}
