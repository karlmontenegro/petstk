//
//  DL_Pet.swift
//  PetsTK
//
//  Created by Karl Montenegro on 18/06/16.
//  Copyright © 2016 Karl Montenegro. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DL_Pet  {
    
    func map(json: JSON)->Array<Pet>{
        var list:Array<Pet> = Array<Pet>()
        
        for (_,elem):(String, JSON) in json {
            let pet = Pet(
                id: elem["id"].int,
                name: elem["name"].stringValue,
                family: elem["family"].stringValue,
                user_id: elem["user_id"].int,
                image_url: elem["image_url"].stringValue,
                is_favorite: elem["is_favorite"].bool,
                url: elem["url"].stringValue)
            
            list.append(pet)
        }
        return list
    }
    
    //MARK: - Aux Functions
        
}
